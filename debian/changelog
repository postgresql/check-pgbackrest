check-pgbackrest (2.4-2) UNRELEASED; urgency=medium


 -- Debian PostgreSQL Maintainers <team+postgresql@tracker.debian.org>  Fri, 05 Jul 2024 09:55:41 +0200

check-pgbackrest (2.4-1) unstable; urgency=medium

  * New upstream version.
    (Closes: #1074786)

 -- Michael Banck <mbanck@debian.org>  Fri, 05 Jul 2024 09:54:42 +0200

check-pgbackrest (2.3-1) unstable; urgency=medium

  * New upstream version.

 -- Christoph Berg <myon@debian.org>  Tue, 14 Jun 2022 16:12:24 +0200

check-pgbackrest (2.2-1) unstable; urgency=medium

  * New upstream version.

 -- Christoph Berg <myon@debian.org>  Mon, 06 Dec 2021 12:24:37 +0100

check-pgbackrest (2.1-1) unstable; urgency=medium

  [ Debian Janitor ]
  * Set upstream metadata fields: Bug-Database, Bug-Submit.
  * Update standards version to 4.6.0, no changes needed.

  [ Christoph Berg ]
  * New upstream version.

 -- Christoph Berg <myon@debian.org>  Fri, 15 Oct 2021 15:04:25 +0200

check-pgbackrest (2.0-1) experimental; urgency=medium

  * New upstream version.
  * Breaks pgbackrest << 2.28 since it uses commands from that version.

 -- Christoph Berg <myon@debian.org>  Fri, 23 Apr 2021 15:37:15 +0200

check-pgbackrest (1.9-2) unstable; urgency=medium

  [ Christoph Berg ]
  * Enhances: pgbackrest.

  [ Chris Lamb ]
  * Use UTC in manpage generation to make build reproducible.
    (Closes: #970851)

 -- Christoph Berg <myon@debian.org>  Thu, 24 Sep 2020 13:51:09 +0200

check-pgbackrest (1.9-1) unstable; urgency=medium

  * Initial release.

 -- Christoph Berg <myon@debian.org>  Tue, 01 Sep 2020 16:30:08 +0200
